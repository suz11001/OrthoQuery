<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$form['Overview'] = array(
        '#type'         => 'fieldset',
        '#title'        => 'Job Submitted',
        '#collapsible'  => FALSE,
        '#description'  => t('your job has been submitted'),
        '#suffix'       => '<br /><br /><h3>Type</h3>'
);

$form['submit_button'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
);