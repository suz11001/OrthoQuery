<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

error_reporting(E_ALL);

function orthoquery_initialize_page() {
  return drupal_get_form('orthoquery_main_form');
}

function orthoquery_main_form($form, &$form_state) {

        //This checks to see if $form_state['page'] variable exists, if not, set it to page 1
        if(!isset($form_state['page'])) {
                $form_state['page'] = 1;  //page 1, else, assume it is set to another number like page 2
        }

        switch($form_state['page']) {
                case 1: 
                    //process the form_state variables
                        include_once('orthoquery_page1.form.inc'); //include page1 form code from file
                        return $form; //return it causes it to be rendered to the page
                        break;
                case 2: 
                    //if page 2 do the following
                        include_once('orthoquery_page2.form.inc'); //show page 2 form
                        return $form;
                        break;
                case 3:
                    //if page 3 do the following
                        include_once('orthoquery_page3.form.inc'); //show page 3 form
                        return $form;
                        break;
                case 4:
                        include_once('orthoquery_demo.form.inc'); //show page 2 form
                        return $form;
                        break;
        }
}

function orthoquery_main_form_submit($form, &$form_state){
        //on submit, this checks to see which page was submitted and whether we need to go to next page by 
        switch($form_state['page']) {
                case 1: 
                    //if submit button value is "Submit"
                    //  then navigate to page 3
                    //else
                    //  navigate to page 4
                    //process the form_state variables
                        $form_state['page'] = 2;
                        break;
                case 2: 
                    //if page 2 do the following
                        $form_state['page'] = 3;
                        break;
        }
//        if(isset($form_state['submit_button'][''])){
//            $form_state['page'] = 4; //This forces user to go to specific page, make sure case 4 exists in orthoquery_main_form($form, &$form_state)
//        }
        //This next line of code is very important, it rebuilds the form_state so that orthoquery_main_form can get back the new values
        //Without this, multi pages won't work
        $form_state["rebuild"] = TRUE; 
}
